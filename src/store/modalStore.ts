import { create } from 'zustand'

export const useStore = create((set: any) => ({
    count: 0,
    dec: () => set((state: any) => ({ count: state.count - 1 })),
    inc: () => set((state: any) => ({ count: state.count + 1 })),
}))
