import { create } from 'zustand'

export const usePCStore = create((set: any) => ({
    isLoading: true,
    imgLoadedCount: 0,
    setLoading: (val: any) => set(() => ({ isLoading: val })),
    imgLoadedCalculate: () => set((state: any) => ({ imgLoadedCount: state.imgLoadedCount + 1 })),
}))
