// import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import './assets/index.css'
import NavPage from './pages/Nav'

ReactDOM.createRoot(document.getElementById('root')!).render(
    <BrowserRouter>
        <NavPage />
    </BrowserRouter>,
)
