import { Outlet, Route, Routes } from 'react-router-dom'
import { Suspense, lazy } from 'react';
import NoMatch from '../components/404.tsx';
import RbimsPage from './Rbims.tsx';
import ResumePage from './Resume.tsx';
import FallbackPage from '../components/skeleton/Fallback.tsx';
import ContactPage from '../components/Contact.tsx';


const Home_page = lazy(() => import('../pages/Home.tsx'))

const Warehouse_page = lazy(() => import('./Warehouse.tsx'))
const School_management_system = lazy(() => import('./school_management_system.tsx'))


export default function NavPage() {


    return (
        <>
            <Routes>
                <Route element={<Layout />}>
                    <Route index element={
                        <Suspense fallback={<FallbackPage />}>
                            <Home_page />
                        </Suspense>
                    } />
                    <Route path="school_management_system" element={
                        <Suspense fallback={
                            < FallbackPage />}>
                            <School_management_system />
                        </Suspense>
                    } />
                    <Route path="rbims" element={
                        <RbimsPage />
                    } />
                    <Route path="resume" element={
                        <ResumePage />
                    } />
                    <Route path="warehouse" element={
                        <Suspense fallback={<FallbackPage />}>
                            <Warehouse_page />
                        </Suspense>
                    } />
                    <Route path="contact" element={
                        <ContactPage />
                    } />
                </Route>
                <Route path="*" element={<NoMatch />} />
            </Routes >
        </>
    )
}


const Layout = () => {
    return (
        <>
            <div className="row mt-5" >
                <div className="col-sm-0 col-md-2 col-lg-2 col-xl-2 "></div>
                <div className="col-sm-12 col-md-8  col-lg-8 col-xl-7 px-5  " style={{ marginBottom: "5%" }}>
                    <Outlet />
                </div>
                <div className="col-md-2 col-lg-2 col-xl-3 "></div>
            </div>
        </>
    )
};



