import { NavLink } from "react-router-dom";

export default function AboutPage() {
    return (
        <>
            <div className="row" style={{ marginTop: "5%" }}>

                <div className="col-md-1"></div>

                <div className="col-md-2">
                    <p style={styles.sublabels}>About</p>
                </div>
                <div className="col-md-8">
                    <p className="">
                        i'm an independent developer based in tacloban city who is
                        proficient in frontend development for web and mobile. i'm new
                        and do not have a lot of professional experience but i can work
                        hard for it to achieve. since i'm new to it, i can work with
                        other people or a team.
                    </p>
                    <p className="mt-3">
                        my social media and other connect is in the <span style={{
                            color: "black",
                            border: "1px solid grey",
                            padding: 2,
                            borderRadius: 5
                        }}>
                            <NavLink to="/resume" >Resume's</NavLink>
                        </span> tab
                    </p>

                </div>
            </div >
        </>
    );
};

const styles: any = {
    sublabels: {
        fontSize: 12.5,
        fontStyle: "normal",
        fontWeight: 300,
        letterSpacing: 1,
        color: "grey",
    }
}

