import CtrPage from "../components/ctr"
import BackSvg from "../assets/logo/back-svgrepo-com.svg"
import { useNavigate } from "react-router-dom";
import TemporaryNotice from "../components/Temporary";
import StateProgremAndAchievements from "../components/StateProgremAndAcheivements";

export default function RbimsPage() {
    const navigate = useNavigate();
    return (
        <div>

            <div className="rbims">
                <img src={BackSvg} alt="" style={{ width: "30px", marginBottom: "25px", cursor: "pointer" }} onClick={() => {
                    navigate("/")
                }} />
                <div className="mb-4">
                    <h5 style={{ letterSpacing: "2px", fontWeight: 400 }}>
                        Rbims Survey App
                        <TemporaryNotice />
                    </h5>
                </div>
                <CtrPage role="Mobile Developer" time_line="Dec 2021 - Oct 2022" client="City government" />

                <StateProgremAndAchievements />

                <div className="">
                    <p className="mb-5">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. At beatae
                        odit, reprehenderit obcaecati accusamus delectus nobis facilis
                        temporibus velit doloribus in inventore fugit! Eaque, maxime
                        delectus qui magni quia doloribus.
                        {"\n"}
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus ratione
                        sequi, quod qui enim excepturi optio alias suscipit temporibus quasi
                        iste, impedit, totam accusantium doloribus. Atque alias debitis veritatis
                        recusandae?
                    </p>
                    <p className="mb-5">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. At beatae
                        odit, reprehenderit obcaecati accusamus delectus nobis facilis
                        temporibus velit doloribus in inventore fugit! Eaque, maxime
                        delectus qui magni quia doloribus.
                    </p>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>       </div >
    )
}
