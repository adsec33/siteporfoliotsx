import { useNavigate } from "react-router-dom";
import BackSvg from "../assets/logo/back-svgrepo-com.svg"
import CtrPage from "../components/ctr"
import PictureCarouselPage from "../components/PictureCarousel";
import TemporaryNotice from "../components/Temporary";
import StateProgremAndAchievements from "../components/StateProgremAndAcheivements";

export default function WarehousePage() {
    const navigate = useNavigate();

    return (
        <div>
            <img src={BackSvg} alt="" style={{ width: "30px", marginBottom: "20px", cursor: "pointer" }} onClick={() => {
                navigate("/")
            }} />

            <div className="rbims " style={{}}>
                <div className="mb-3 " >
                    <h5 style={{ letterSpacing: "2px", fontWeight: 400 }}>
                        Warehouse App ( Porfolio )
                        <TemporaryNotice />
                    </h5>
                </div>
                <CtrPage client="Porfolio" role="Developer" time_line="July 2023 - Oct 2023" />

                <PictureCarouselPage from="warehouse" />

                <StateProgremAndAchievements />

                <div className="mt-3">
                    <p className="mb-3">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. At beatae
                        odit, reprehenderit obcaecati accusamus delectus nobis facilis
                        temporibus velit doloribus in inventore fugit! Eaque, maxime
                        delectus qui magni quia doloribus.
                        {"\n"}
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus ratione
                        sequi, quod qui enim excepturi optio alias suscipit temporibus quasi
                        iste, impedit, totam accusantium doloribus. Atque alias debitis veritatis
                        recusandae?
                    </p>
                    <p className="mb-3">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. At beatae
                        odit, reprehenderit obcaecati accusamus delectus nobis facilis
                        temporibus velit doloribus in inventore fugit! Eaque, maxime
                        delectus qui magni quia doloribus.
                    </p>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
        </div >
    )
}


