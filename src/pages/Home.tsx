import FullnamePage from "../components/Fullname";
import ProficientPage from "../components/Proficient";
import AboutPage from "./About";
import MobileSvg from "../assets/logo/mobile-alt-2-svgrepo-com.svg";
import WebSvg from "../assets/logo/web-svgrepo-com.svg";
import ApiSvg from "../assets/logo/api-svgrepo-com.svg";
import InfoSvg from "../assets/images/exclamation_black.svg";
import UbuntuSvg from "../assets/images/ubuntu1_black.svg";
import LaravelSvg from "../assets/images/laravel_black.svg";
import MysqlSvg from "../assets/images/mysql_black.svg";
import ReactjsSvg from "../assets/images/react_black.svg";
import GitlabSvg from "../assets/images/gitlab_black.svg";
import VimSvg from "../assets/images/vim_black.svg";
import { NavLink } from "react-router-dom";

let uuid = 1;
let dataImg = [
    { id: uuid++, name: "Gitlab", image: GitlabSvg },
    { id: uuid++, name: "Ubuntu", image: UbuntuSvg },
    { id: uuid++, name: "MySql", image: MysqlSvg },
    { id: uuid++, name: "Laravel", image: LaravelSvg },
    { id: uuid++, name: "Vim", image: VimSvg },
    { id: uuid++, name: "React Js", image: ReactjsSvg },
    // { id: uuid++, name: "Figma", image: FigmaSvg },
];

const pwp = [
    {
        id: uuid++,
        title: "Rbims Survey App",
        descrption: "Student register and take exams",
        tech_use: ["mobile", "SQLite"],
        link: "/rbims",
    },
    {
        id: uuid++,
        title: "School Management System",
        // descrption: "School System",
        descrption: "",
        tech_use: ["web", "api"],
        link: "/school_management_system",
    },
   
    // {
    //     id: uuid++,
    //     title: "School Entrance Exam",
    //     descrption: "Student regiter and take exams",
    //     tech_use: ["web", "api"],
    //     link: "/entrance_exam",
    // },
    // {
    //     id: uuid++,
    //     title: "Warehouse App",
    //     descrption: "Student regiter and take exams",
    //     tech_use: ["mobile", "api"],
    //     link: "/warehouse",
    // },
];

export default function HomePage() {

    return (
        <div>
            <FullnamePage />
            <AboutPage />
            <ProficientPage />

            <div className="row " style={{ marginTop: "30px" }}>
                <div className="col-md-1"></div>
                <div className="col-md-2">
                    <p style={styles.sublabels}>Personal / Work Projects</p>
                </div>
                <div className="col-md-8">
                    <div className="">
                        <img
                            src={InfoSvg}
                            alt="info"
                            className=""
                            style={{ width: 20, float: 'right' }}
                        />
                    </div>

                    <ul className="list-group list-group-flush">
                        {
                            pwp.map((item: any) =>
                                <li className="list-group-item" style={styles.listpwp}
                                    key={item.id}
                                >
                                    {/* <NavLink to={item.link} key={item.id} >*/}
                                    <NavLink to={item.link} key={item.id}> 
                                    <p> {item.title} <span> {item.title === "Warehouse App" ? "( Porfolio )" : ''}</span></p>
                                    <p className="">
                                        {item.descrption}
                                    </p>

                                    <div className="mb-3"  >
                                        {item.tech_use.map((item2: any, index2: number) => (
                                            item2 === "mobile" ? <img key={index2} src={MobileSvg} alt={item2} /> : ''
                                        ))}

                                        {item.tech_use.map((item2: any, index2: number) => (
                                            item2 === "web" ? <img key={index2} src={WebSvg} alt={item2} /> : ''
                                        ))}

                                        {item.tech_use.map((item2: any, index2: number) => (
                                            item2 === "api" ? <img key={index2} src={ApiSvg} alt={item2} /> : ''
                                        ))}
                                    </div>

                                    </NavLink> 
                                    {/*  </NavLink> */}

                                </li>
                            )
                        }
                    </ul>
                </div>
            </div>
            <div className="row " style={{ marginTop: "20px" }}>
                <div className="col-md-1"></div>
                <div className="col-md-2">
                    <p style={styles.sublabels}>Stack</p>
                </div>
                <div className="col-md-8  ">
                    <div className="row">
                        {
                            dataImg.map((item: any) =>
                                <div
                                    className="text-center  "
                                    style={{
                                        width: "22%",
                                        // fontSize: "11px",
                                        padding: "2%",
                                        marginLeft: "2.3%",
                                        marginBottom: "15px",
                                        backgroundColor: "#EEEEEE",
                                        borderRadius: "15px "
                                    }}
                                    key={item.id}
                                >
                                    <img
                                        src={item.image}
                                        alt="info"
                                        className=""
                                        style={{ maxWidth: "100%", maxHeight: "100%" }}
                                    />
                                    <span>{item.name}</span>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div >
            <br />  <br />
            <br />  <br />
        </div >
    )
}

const styles: any = {
    sublabels: {
        fontSize: 12.5,
        fontStyle: "normal",
        fontWeight: 300,
        letterSpacing: 1,
        color: "grey",
    },
    listpwp: {
        marginBottom: "5px",
        borderRadius: "5px",
        padding: 0
    },
}


