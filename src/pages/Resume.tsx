import ProficientPage from "../components/Proficient"
import BackSvg from "../assets/logo/back-svgrepo-com.svg"
import { useNavigate } from "react-router-dom";


export default function ResumePage() {
    const navigate = useNavigate();
    return (
        <>

            <img src={BackSvg} alt="" style={{ width: "30px", marginBottom: "3%", cursor: "pointer" }} onClick={() => {
                navigate("/")
            }} />

            <div className="resume">
                <div className="row">
                    <h5
                        style={{ letterSpacing: "2px", fontWeight: 400, marginBottom: "15px" }}
                    >
                        Resume
                    </h5>

                    <div className="col-md-1"></div>
                    <div className="col-md-2">
                        <p style={styles.sublabels} >Education</p>
                    </div>
                    <div className="col-md-8">
                        <p style={styles.sublabels}>Secondary School</p>
                        <p className="">Sagkahan Nation High School</p>
                        <p className="mb-3">2008-2009</p>

                        <p style={styles.sublabels} >Bachelor Of Technology</p>
                        <p className="">Asian Development Foundation College Tacloban City</p>
                        <p className="mb-3">2008-2009</p>

                        <p className="">System Technology Institute</p>
                        <p className="mb-3">2011-2014</p>
                        <span style={styles.cert} className="">**CERTIFICATE OF RECOGNITION</span>
                        <p style={styles.award} className="mb-3">NOMINATED IN THE BEST PROGRAMMER CATEGORY</p>

                        <p className="">Aclc College Tacloban City</p>
                        <p className="">2016-2022</p>
                        <span style={styles.cert} >*CERTIFICATE OF RECOGNITION</span>
                        <p style={styles.award} >
                            FIRST PLACE IT SKILLS COMPETITIONS MOBILE PROGRAMMING
                        </p>
                    </div>
                </div>
                <ProficientPage />

                <div className="row mt-5">

                    <div className="col-md-1"></div>
                    <div className="col-md-2">
                        <p style={styles.sublabels} >Experience</p>
                    </div>
                    <div className="col-md-8">
                        <p style={styles.sublabels}>Mobile Developer</p>
                        <p className="">Audacious Site Enterprice</p>
                        <p className="mb-3">Nov-2020 - Jan 2021</p>

                        <p style={styles.description}>Mobile design & database administration</p>
                        <p style={styles.description}>
                            Manage gathering data using the application
                        </p>
                        <p style={styles.description} className="mb-3">Exporing data for processing</p>

                        <p style={styles.sublabels}>Jr Backend Developer</p>
                        <p >Telmo Solutions</p>
                        <p className="mb-3">Jan 2023 - May 2023</p>

                        <p style={styles.description} >Frontend Ionic</p>
                        <p style={styles.description} >Laravel Framework for backend</p>
                        <p style={styles.description} >Saas Application</p>
                    </div>
                </div>
            </div>

        </>

    );
}
const styles: any = {
    sublabels: {
        color: "gray",
        letterSpacing: "0.5px",
        fontWeight: 300,
    },
    cert: {
        fontSize: 11,
        color: "gray",
        letterSpacing: "0.5px",
    },
    award: {
        fontSize: 11,
    },
    description: {
        fontSize: "12px",
        letterSpacing: "0.5px",
    }

}

