import CtrPage from "../components/ctr";
import BackSvg from "../assets/logo/back-svgrepo-com.svg";
import { useNavigate } from "react-router-dom";
import PictureCarouselPage from "../components/PictureCarousel";
import TemporaryNotice from "../components/Temporary";
import StateProgremAndAchievements from "../components/StateProgremAndAcheivements";

export default function SchoolManagementSystemPage() {
    const navigate = useNavigate();
    return (
        <>
            <div className="">
                <div className="col-md-12 p-1">
                    <img src={BackSvg} alt="" style={{ width: "30px", marginBottom: "15px", cursor: "pointer" }} onClick={() => {
                        navigate("/")
                    }} />
                    <div className="">
                        <h5
                            className="mb-3"
                            style={{
                                letterSpacing: "2px", fontWeight: 400,
                            }}
                        >
                            School Management System
                            <TemporaryNotice />

                        </h5>

                    </div>
                    <CtrPage role="Developer" time_line="Feb 2024 - Aug 2024" client="Private" />
                    <PictureCarouselPage from="school_management_system" />

                    <StateProgremAndAchievements />

                    <div className="mt-3">
                        <p className="mb-3">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. At beatae
                            odit, reprehenderit obcaecati accusamus delectus nobis facilis
                            temporibus velit doloribus in inventore fugit! Eaque, maxime
                            delectus qui magni quia doloribus.
                            {"\n"}
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus ratione
                            sequi, quod qui enim excepturi optio alias suscipit temporibus quasi
                            iste, impedit, totam accusantium doloribus. Atque alias debitis veritatis
                            recusandae?
                        </p>
                        <p className="mb-3">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. At beatae
                            odit, reprehenderit obcaecati accusamus delectus nobis facilis
                            temporibus velit doloribus in inventore fugit! Eaque, maxime
                            delectus qui magni quia doloribus.
                        </p>

                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </div>

                </div >
            </div >
        </>
    );
}
