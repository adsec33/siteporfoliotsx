

import Skeleton from "react-loading-skeleton";

export default function FallbackPage() {

    return (

        <div>
            <Skeleton style={{ width: "20%" }} />
            <div className="row" style={{ marginTop: "5%" }}>
                <div className="col-md-1"></div>
                <div className="col-md-2">
                    <Skeleton />
                </div>
                <div className="col-md-8">
                    <p className="">
                        <Skeleton />
                        <Skeleton />
                        <Skeleton />
                    </p>
                </div>
            </div >
            <div className="row" style={{ marginTop: "5%" }}>
                <div className="col-md-1"></div>
                <div className="col-md-2">
                    <Skeleton />
                </div>
                <div className="col-md-8">
                    <p className="">
                        <Skeleton />
                        <Skeleton />
                        <Skeleton />
                    </p>
                </div>
            </div>
            <div className="row" style={{ marginTop: "5%" }}>
                <div className="col-md-1"></div>
                <div className="col-md-2">
                    <Skeleton />
                </div>
                <div className="col-md-8">
                    <p className="">
                        <Skeleton />
                        <Skeleton />
                        <Skeleton />
                    </p>
                </div>
            </div>
        </div>
    )
}



