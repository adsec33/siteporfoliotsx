import Skeleton from "react-loading-skeleton";

export default function HomeFallbackPage() {

    return (
        <div className="row HomeFallbackPage">
            <center>
                <Skeleton circle={true} style={{
                    width: "100px",
                    height: "100px",
                    borderRadius: "50%",
                    overflow: "hidden",
                    display: "inline-block",
                    verticalAlign: "middle",
                    marginBottom: "10px",
                    marginTop: "5%",
                }} />

                <div className="" style={{
                    justifyContent: "center",
                    alignItems: "center",
                    display: "flex",
                    marginTop: "15px",
                    marginBottom: "15px"
                }}>
                    <Skeleton className="text-dark   m-1" style={styles.contact_info} />

                    <Skeleton className="text-dark   m-1" style={styles.contact_info} />
                </div>
            </center>


        </div>
    )
}


const styles = {
    contact_info: {
        width: "150px",
        backgroundColor: "#eeeeee",
        padding: "5px",
        fontSize: "14px",
        borderRadius: "6px",
        cursor: "pointer",
    }
};
