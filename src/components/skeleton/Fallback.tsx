
import Skeleton from "react-loading-skeleton";

export default function FallbackPage() {

    return (
        <div className="" style={{ marginTop: "15%" }}>
            <Skeleton style={{ width: "20%" }} />

            <div className="" style={{ marginBottom: "20px" }}>
                <div className="row">
                    <div
                        className="col-md-5"
                        style={{ fontSize: "12px", textAlign: "left" }}>
                        <div className="row">
                            <p className="col-3">
                                <Skeleton />
                            </p>
                            <span className="col">
                                <Skeleton style={{ width: "20%" }} />
                            </span>
                        </div>
                        <div className="row">
                            <p className="col-3">
                                <Skeleton />
                            </p>
                            <span className="col">
                                <Skeleton style={{ width: "20%" }} />
                            </span>
                        </div>
                        <div className="row">
                            <p className="col-3">
                                <Skeleton />
                            </p>
                            <span className="col">
                                <Skeleton style={{ width: "20%" }} />
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row" style={{ marginTop: "5%" }}>
                <Skeleton style={{ height: "100px", marginBottom: "25px" }} />
                <br /> <br />

                <Skeleton />
                <Skeleton />
                <Skeleton />
                <br /> <br />
                <Skeleton />
                <Skeleton />
                <Skeleton />
                <br /> <br />
                <Skeleton />
            </div >
        </div>
    )
}
