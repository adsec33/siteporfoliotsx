import 'react-loading-skeleton/dist/skeleton.css'

export default function NoMatch() {
    return (
        <div className="" style={styles.ceenterScreen}>
            <center>
                <i className="fa-regular fa-face-sad-tear" style={{ fontSize: "75px", marginTop: "15%" }}></i>
                <p style={{ marginTop: "15px", fontSize: "20px" }}>
                    Sorry, there's nothing here.
                </p>
            </center>
        </div>

    );
};


const styles: any = {
    ceenterScreen: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        minHeight: "100vh",
    },
    contact_info: {
        width: "150px",
        backgroundColor: "#eeeeee",
        padding: "5px",
        fontSize: "14px",
        borderRadius: "6px",
        cursor: "pointer",
    }
}


