import Modal from 'react-bootstrap/Modal';
import PictureCarouselPage from './PictureCarousel';
import BackSvg from "../assets/logo/back-svgrepo-com.svg";

export default function ModalPage({ from, handleShow, show }: any) {
    const handleClose = () => {
        handleShow(false);
    }

    return (
        <>
            <Modal
                show={show}
                // onHide={handleShow}
                dialogClassName="modal-90w"
                aria-labelledby="example-custom-modal-styling-title"
                size={from === "warehouse" ? 'lg' : from === "school_management_system" ? 'xl' : 'sm'}

            >
                <img src={BackSvg} alt="" style={{ width: "30px", marginLeft: "1%", marginTop: "1%", cursor: "pointer" }} onClick={handleClose} />

                <Modal.Body style={{
                }}>
                    <PictureCarouselPage from={from} />
                </Modal.Body>
            </Modal>

        </>
    );
}

