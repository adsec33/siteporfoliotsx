export default function CtrPage({
    client = "",
    time_line = "",
    role = "",
}: {
    client: string;
    time_line: string;
    role: string;
}) {
    return (
        <>
            <div className="" style={{ marginBottom: "20px" }}>
                <div className="row">
                    <div
                        className="col-md-5"
                        style={{ fontSize: "12px", textAlign: "left" }}>
                        <div className="row">
                            <p className="col-3">Client</p>
                            <span className="col">
                                {client === undefined ? "None" : client}
                            </span>
                        </div>
                        <div className="row">
                            <p className="col-3">Timeline</p>
                            <span className="col">
                                {time_line === undefined ? "None" : time_line}
                            </span>
                        </div>
                        <div className="row">
                            <p className="col-3">Role</p>
                            <span className="col">
                                {role === undefined ? "None" : role}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
