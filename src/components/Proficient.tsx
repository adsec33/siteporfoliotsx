


export default function ProficientPage() {

    return (
        <div className="proficient" style={{ marginTop: "30px" }}>
            <div className="row">

                <div className="col-md-1 "></div>
                <div className="col-md-2 ">
                    <p style={styles.sublabels}>Proficient</p>
                </div>
                <div className="col-md-8">
                    <p className="m-0">B4a, html, css, javascript, react js</p>
                    <p className="">Mysql, php, laravel</p>
                    <p className="">Linux, windows, git, vim</p>
                </div>
            </div>
        </div>

    );

}


const styles: any = {
    sublabels: {
        fontSize: 12.5,
        fontStyle: "normal",
        fontWeight: 300,
        letterSpacing: 1,
        color: "grey",
    }
}

