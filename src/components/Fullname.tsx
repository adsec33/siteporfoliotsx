import ppic from "../assets/delosreyes/a1.jpg";
import { NavLink } from "react-router-dom";

import Skeleton from "react-loading-skeleton";
import 'react-loading-skeleton/dist/skeleton.css'
import { useCallback, useEffect, useState } from "react";

export default function FullnamePage() {
    const [isLoading, toggleContentLoading] = useState(true);

    let uid = 1;
    const smdata = [
        { id: uid++, cn: "fa-brands fa-gitlab", to: "https://gitlab.com/adsec33" },
        { id: uid++, cn: "fa-brands fa-linkedin-in", to: "https://www.linkedin.com/in/delos-reyes-al-fritz-8b890527a/" },
        { id: uid++, cn: "fa-brands fa-facebook-f", to: "https://www.facebook.com/secugal/" },
    ]
    const onLoad = useCallback(() => {
        toggleContentLoading(false);
    }, []);

    useEffect(() => {
        toggleContentLoading((state) => !state);
    }, []);


    return (
        <div className="row fullname " >
            <center>
                {isLoading ??
                    <Skeleton circle={true} style={{
                        width: "100px",
                        height: "100px",
                        borderRadius: "50%",
                        overflow: "hidden",
                        display: "inline-block",
                        verticalAlign: "middle",
                        marginBottom: "10px",
                        marginTop: "5%",
                    }} />
                }
                <img style={styles.circular_image} src={ppic} alt="profile_img" onLoad={onLoad} />
                <div className="col-md-8 mb-1">
                    <h1 style={{ fontSize: 19, letterSpacing: 1.5, marginBottom: "5px", fontWeight: 400 }}>
                        De los Reyes, Al fritz S.
                    </h1>
                    <p style={{ color: "grey", fontSize: "13px", letterSpacing: "5px", fontWeight: 400 }}>
                        Frontend Developer
                    </p>
                    <div className="mt-2">
                    </div>
                </div>

                <div className="mt-3">
                    {smdata.map((item: any, ndx: number) =>
                        <a href={item.to} target="_blank" key={ndx}>
                            <i style={styles.social_media} className={item.cn} key={ndx}></i>
                        </a>
                    )}
                </div>

                <div className="" style={{
                    justifyContent: "center",
                    alignItems: "center",
                    display: "flex",
                    marginTop: "15px",
                    marginBottom: "15px"
                }} hidden>
                    <NavLink to="/contact" className="text-dark   m-1" style={styles.contact_info} >
                        Contact me
                        <i className="fa-regular fa-envelope" style={{ marginLeft: "15px" }
                        } ></i>
                    </NavLink>

                    <p className="text-dark  m-1" style={styles.contact_info}
                        onClick={() => { navigator.clipboard.writeText("qweqwewq") }}

                    >
                        Copy email
                        <i className="fa-regular fa-copy  " style={{ marginLeft: "15px" }}></i>
                    </p>
                </div>
            </center >
        </div >
    );
}

const styles = {
    social_media: {
        margin: "5px",
        marginTop: 0,
        borderRadius: "5px",
        fontSize: "20px",
    },
    available: {
        color: "teal",
        marginLeft: "3px",
        fontSize: "11px",
        letterSpacing: "2.5px",

    },
    circular_image: {
        width: "100px",
        height: "100px",
        borderRadius: "50%",
        overflow: "hidden",
        backgroundColor: "#eeeeee",
        display: "inline-block",
        verticalAlign: "middle",
        marginBottom: "10px",
        marginTop: "5%",
    },
    contact_info: {
        width: "150px",
        backgroundColor: "#eeeeee",
        padding: "5px",
        fontSize: "14px",
        borderRadius: "6px",
        cursor: "pointer",
    }
};
