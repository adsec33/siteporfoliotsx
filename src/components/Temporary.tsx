export default function TemporaryNotice() {
    return (
        <span style={{ marginLeft: "15px", fontSize: ".7rem", color: "gray" }}>( Information not yet Complete, temporary. )</span>
    )
}
