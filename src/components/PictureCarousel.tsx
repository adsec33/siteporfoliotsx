import mob1 from "../assets/mobs_mobile/a1.png"
import mob2 from "../assets/mobs_mobile/a2.png"
import mob3 from "../assets/mobs_mobile/a3.png"
import mob4 from "../assets/mobs_mobile/a4.png"
import mob5 from "../assets/mobs_mobile/a5.png"
import mob6 from "../assets/mobs_mobile/a6.png"
import mob7 from "../assets/mobs_mobile/a7.png"
import mob8 from "../assets/mobs_mobile/a8.png"
import mob9 from "../assets/mobs_mobile/a9.png"
import mob10 from "../assets/mobs_mobile/a10.png"
import mob11 from "../assets/mobs_mobile/a11.png"
import mob12 from "../assets/mobs_mobile/a12.png"
import mob13 from "../assets/mobs_mobile/a13.png"
import mob14 from "../assets/mobs_mobile/a14.png"
import mob15 from "../assets/mobs_mobile/a15.png"
import mob16 from "../assets/mobs_mobile/a16.png"
import mob17 from "../assets/mobs_mobile/a17.png"
import mob18 from "../assets/mobs_mobile/a18.png"
import mob19 from "../assets/mobs_mobile/a19.png"
import mob20 from "../assets/mobs_mobile/a20.png"

import sys_img_1 from "../assets/image_school_management_system/a1.png"
import sys_img_2 from "../assets/image_school_management_system/a2.png"
import sys_img_3 from "../assets/image_school_management_system/a3.png"

import Carousel from 'react-bootstrap/Carousel';
import ModalPage from "./Modal"
import { useState } from "react"
import { useStore } from "../store/modalStore"

import 'react-loading-skeleton/dist/skeleton.css'
import Skeleton from "react-loading-skeleton"
import { usePCStore } from "../store/PictureCarouselStore"

export default function PictureCarouselPage({ from }: any) {
    let uuid = 1;

    const [show, setShow] = useState<boolean>(false)
    const { count, inc, dec } = useStore();



    const handleShow = (val: any) => {
        if (val) {
            inc();
        } else {
            dec()
        }
        setShow(val)
    }

    const mobdata = [
        { id: uuid, name: mob1 },
        { id: uuid, name: mob2 },
        { id: uuid, name: mob3 },
        { id: uuid, name: mob4 },
        { id: uuid, name: mob5 },
        { id: uuid, name: mob6 },
        { id: uuid, name: mob7 },
        { id: uuid, name: mob8 },
        { id: uuid, name: mob9 },
        { id: uuid, name: mob10 },
        { id: uuid, name: mob11 },
        { id: uuid, name: mob12 },
        { id: uuid, name: mob13 },
        { id: uuid, name: mob14 },
        { id: uuid, name: mob15 },
        { id: uuid, name: mob16 },
        { id: uuid, name: mob17 },
        { id: uuid, name: mob18 },
        { id: uuid, name: mob19 },
        { id: uuid, name: mob20 },
    ]

  const smsdata = [
        { id: uuid, name: sys_img_1},
        { id: uuid, name: sys_img_2},
        { id: uuid, name: sys_img_3 },
       ]


    const pcstore = usePCStore();

    const handleImgLoad = (val: boolean) => {
        pcstore.setLoading(val)
    }

    const mobile = () => {
        return (
            <div className="m-1 col-sm-12 col-xxl-8 col-md-8" style={{
                boxShadow: '0px 2px 10px #d4d4d4',
                borderRadius: "10px",

            }}>
                <Carousel fade indicators={false} >
                    {
                        mobdata.map((item: any, ndx: number) =>
                            <Carousel.Item key={ndx}>
                                <img src={item.name} style={{ width: "100%" }}
                                    onClick={() => count === 0 ? handleShow(true) : null}
                                    loading="lazy"
                                    alt="loading..."
                                    onLoad={() => handleImgLoad(false)}
                                />
                            </Carousel.Item>
                        )
                    }
                </Carousel>
            </div>
        )
    }

    const web = () => {
        return (
            <>
                <div className=" col-sm-12 col-xxl-12 col-md-12" style={{
                    boxShadow: '0px 2px 10px #d4d4d4',
                    borderRadius: "10px",

                }}>                    <Carousel fade indicators={false} >
                        {
                            smsdata.map((item: any, ndx: number) =>
                                <Carousel.Item key={ndx}
                                >
                                    <img src={item.name} style={{ width: "100%" }}
                                        onClick={() => count === 0 ? handleShow(true) : null}
                                        loading="lazy"
                                        onLoad={() => handleImgLoad(false)}
                                        alt="loading..."
                                    />
                                </Carousel.Item>
                            )
                        }
                    </Carousel>
                </div >
            </>
        )
    }

    return (
        <>
            <div className="picturecarousel " style={{ marginBottom: "20px" }} >
                <ModalPage from={from} handleShow={handleShow} show={show} />

                {pcstore.isLoading ?
                    <Skeleton style={{ height: "100px", marginBottom: "25px" }} /> : null}

                {from === "school_management_system" ? 
                    <img src={sys_img_1} style={{ width: "100%" }}
                        onClick={() => count === 0 ? handleShow(true) : null}
                        loading="lazy"
                        onLoad={() => handleImgLoad(false)}
                        alt="loading..."
                        hidden={count >= 1 ? true : false}
                    /> 
                : ""}

                <center
                    hidden={count === 0 ? true : false}
                >
                    {from === "warehouse" ? mobile() : from === "school_management_system" ? web() : ""}
                </center>
            </div >
        </>

    );
}

