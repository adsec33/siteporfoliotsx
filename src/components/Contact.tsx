
import BackSvg from "../assets/logo/back-svgrepo-com.svg"
import { useNavigate } from "react-router-dom";

export default function ContactPage() {
    const navigate = useNavigate();

    return (
        <>
            <img src={BackSvg} alt="" style={{ width: "30px", marginBottom: "3%", cursor: "pointer" }} onClick={() => {
                navigate("/")
            }} />

            <div className="contact" style={{ marginTop: "0px" }}>
                <div className="col-md-1"></div>

                <div className="col-md-11">
                    <h5 className="mb-0">Contact <span style={{ fontSize: ".8rem", color: "gray", letterSpacing: ".0875rem" }}>( Not Working for now )</span></h5>
                    <p className="" style={{ fontSize: "11px" }}>
                        Let's work together
                    </p>

                    <form className="mt-4">
                        <div className="form-group">
                            <input
                                type="email"
                                className="form-control "
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                placeholder="Email"
                            />
                            <small
                                id="emailHelp"
                                className="form-text text-muted"
                                style={{ fontSize: "11px" }}>
                                We'll never share your email with anyone else.
                            </small>
                        </div>
                        <div className="form-group mt-3">
                            <textarea
                                className="form-control "
                                id="exampleFormControlTextarea1"
                                placeholder="Message"
                                rows={5}></textarea>
                        </div>
                        <div className="form-check mt-1">
                            <input
                                type="checkbox"
                                className="form-check-input"
                                id="exampleCheck1"
                            />
                            <label
                                className="form-check-label"
                                htmlFor="exampleCheck1"
                                style={{
                                    marginLeft: "10px",
                                    marginTop: "4px",
                                    letterSpacing: ".5px",
                                    fontSize: "11px",
                                }}>
                                Subscribe
                            </label>
                        </div>
                        <button type="submit" className="btn  w-100 mt-3">
                            Submit
                        </button>

                        <center>
                            <small
                                id="emailHelp"
                                className="form-text text-muted"
                                style={{ fontSize: "11px" }}>
                                Thanks, response will be soon
                            </small>

                        </center>
                    </form>
                </div>
            </div>
        </>
    );
}
